Use this repository with the slides posted on CMS.

## Some considerable notes
1. Teaching slides are a bit different from the one on FLM so be sure to update!
2. This course is fundamental to every developer since working with DB are essential in software developments & analytics. So please study it for you! It gonna be asked in job/internship interviews/tests for freshers.
3. Random SQL snippets on the Internet may outdated (or from other SQL dialects) lead to incompatibility so chose wisely.

## Preparation
1. For Server side, use [Microsoft® SQL Server® **2017** _Express_](https://www.microsoft.com/en-us/download/details.aspx?id=55994) by the normal installation method (Don't Docker)
2. For Client side (and Management tool), use [SQL Server Management Studio (SSMS)](https://docs.microsoft.com/en-us/sql/ssms/download-sql-server-management-studio-ssms?view=sql-server-ver15) (be sure _newer 2017 version_ for compatibility)

Why ? DBMSs are similar but the course skeleton was designed & finalized by GW academic team to use Microsoft SQL Server **2017**, slides also were prepared best for SQL Server & SSMS tool user interface.

##  Issues
You might got trouble while installing or running query. Don't worry or give up too soon. 
1. Search over Internet first, pick relevant answers from trustworthy source (to avoid crappy websites with full of ads) like stackoverflow to try and solve.
2. In the case you couldn't find any proper help. Post/open issues here (this repository) so everybody (including me - Lecturer) can see, discuss and help. Openly raised issues benefit who may also got same trouble like you afterwards. _Don't be shy!_ Also _Never let your Ego stop you from Learning_

## Lesson Record Videos
Record Videos gonna be upload to YouTube and links shared from here:

### 2021. Fall
1. [Day 1 video](https://drive.google.com/file/d/1aLACvSSgXrmtcy8ob-oJhkZZ1iqu-kil/view?usp=sharing) (Sorry for lengthy, I'll improve next time)
Don't forget to checkout exercises
2. [Day 2 video 1](https://drive.google.com/file/d/196t5wBxqXfWb4NtWR4ri-kcIv_l3CHY0/view?usp=sharing) and
[Day 2 video 2](https://drive.google.com/file/d/1wxO0zBFunnX_fxLMtbNyfuNnVZWTC9cy/view?usp=sharing) . Also checkout exercises from Day (still related to Day 2). SQL script uploaded too.
3. [Day 3 video 1](https://drive.google.com/file/d/1ktLt1p-LVfbTJtyNi38Wy782p4HoAHrl/view?usp=sharing),
[Day 3 video 2](https://drive.google.com/file/d/1sLNzNo4fIHTB11YJRewa2XijCS1EFdNC/view?usp=sharing) and [Day 3 video 3](https://drive.google.com/file/d/1289Dan9JXG_VkZwDMNfYt-UMoR05gvEG/view?usp=sharing). SQL script uploaded too.
4. [Day 4 video 1](https://drive.google.com/file/d/12Rpcqsjvxq9GJb5JWMk-DEubFWytFp_5/view?usp=sharing),
[Day 4 video 2](https://drive.google.com/file/d/163oO9dI2l-ZQj2nnbMxMoaL6elekvBJp/view?usp=sharing). One big exercise with 3 main problems (using 3 databases) was uploaded in Folder Day 4. 
The database backup used on lesson (using "restore" feature on SSMS to restore) is [here](https://1drv.ms/u/s!Atg_Cq2yco1290GIzOzEVYxGaKrf?e=kf7lcq)
5. [Day 5 video 1](https://drive.google.com/file/d/1uOa-rQqjxrlp1kXja_30o6HtgbX2twUC/view?usp=sharing),
[Day 5 video 2](https://drive.google.com/file/d/1Atsls_7UPUMjNLJAOCD8dtMWaZUTSLl5/view?usp=sharing). For those who has problem with my english. Take a look at this [webpage](https://viblo.asia/p/chuan-hoa-co-so-du-lieu-quan-he-bWrZnLrY5xw), quite good, brief with practical example. 
6. [Day 6 video 1](https://drive.google.com/file/d/13BmclzgQOwORs_qsHzmhIgXAtkVgGqDB/view?usp=sharing),
[Day 6 video 2](https://drive.google.com/file/d/1-pEf2d7McQhh1UMRdE9mowXjPRV_pBq1/view?usp=sharing). 
These are [**Topics for Assigment**](https://docs.google.com/spreadsheets/d/1GxGfB9CF0orIQF3Fi31k_npgRYul8C5MYF2l4Pyhs2I/edit?usp=sharing). Survey (light research first, consider carefully) before writing down your name on it (Login using your school account!). Thanks
7. [Day 7 video 1](https://drive.google.com/file/d/1pZt1tJlFFjAt4tUKryjv0ZOHdhR5a0YJ/view?usp=sharing),
[Day 7 video 2](https://drive.google.com/file/d/1F-G09SjbLQ7tW-ludg41jiqP26qat5tP/view?usp=sharing). Please be hurry in deciding topic and working ASM ! Prepare your business questions (is it reasonable?, is this not ok?.. blah) to the lecturer to check if it's good. _Victory Loves Preparation_
8. [Day 8 video 1](https://drive.google.com/file/d/1Od1v8CgNDuFpVoDzoAy-qlebHBZMrW-H/view?usp=sharing). Please be hurry in deciding topic and working ASM ! Prepare your business questions (is it reasonable?, is this not ok?.. blah) to the lecturer to check if it's good. _Victory Loves Preparation_
9. [Day 9 video 1](https://drive.google.com/file/d/1IvNEFMDiT21CjvqX4h-XwEpTwybtW_RV/view?usp=sharing).
[Day 9 video 2](https://drive.google.com/file/d/1sgsDLT_lmFg9EgH3iwGrg-BiF3saeNZb/view?usp=sharing).
Check folder **Day 9** for permission, users, roles in the _permission.sql_ script.

### 2022. Spring.1 Videos here.
1. [Day3 Video - GCD1001](https://drive.google.com/file/d/1nLwA1Gb0UYldLm86VHf5zEsPEyP06D4t/view?usp=sharing)
2. [Day4 Video - GCD1001](https://drive.google.com/file/d/1Y8O1MpAOSItd9XOTnIx4rFIn2YQjqCYo/view?usp=sharing)
3. Struggling with find entity with max/avg/... value of each groups, try this
```sql
SELECT a.id, a.rev, a.contents
FROM YourTable a
INNER JOIN (
    SELECT id, MAX(rev) rev
    FROM YourTable
    GROUP BY id
) b ON a.id = b.id AND a.rev = b.rev
```
4. Transactions: Check [this video (VN)](https://www.youtube.com/watch?v=aOo9CesvUYM) ! Understand first.
5. Trigger: Check [this video (VN)](https://www.youtube.com/watch?v=sfX95kfoAas) ! Learn it at home before class. I just review very shortly on class.
6. The problem/activity of withdraw money from a bank account:

    6.1. Prepare Data: We have Customers and Withdrawals to record all withdrawal history.
    ```sql
    create table Customers
    (UserID int primary key,
    FirstName nvarchar(20),
    DoB date,
    IsFrozen bit default 0,
    Balance int)

    create table Withdrawals
    (WID int identity primary key,
    UserID int references Customers(UserID),
    WDate date,
    WAmount int)

    insert into Customers values
    (1, 'Thao', '1995-01-01', 0, 1000),
    (2, 'Do', '1996-01-01', 0, 500)
    ```

    6.2. Create Stored Procedure for withdraw money (Learn *Transaction* first to understand)
    
    Firstly, check validation. If it's okay, insert 1 row to Withdrawals and then update the balance down.
    ```sql
    create procedure sp_withdraw @Who int, @HowMuch int, @Result varchar(10) output
    as
    BEGIN -- begin of SP
    -- check the condition first
    declare @balance int --declare var
    SET @balance = 
        (select balance from Customers where UserID = @Who)
    IF @balance < @HowMuch
        BEGIN -- begin of IF
        SET @Result = 'Not enough balance' -- for output
        select 'Not enough balance' -- for display
        return -- exit now, we're done here.
        END -- end of IF
    ELSE
        BEGIN -- begin of ELSE (after IF)
        begin transaction -- begin of transaction
        -- insert to Withdrawals to record history
        insert into Withdrawals values (@Who, GETDATE(), @HowMuch);
        -- change balance to reflect new balance (updating)
        update Customers set balance = @balance - @HowMuch where UserID = @Who;
        commit transaction; -- transaction END here.
        SET @Result = 'Successful' -- for output
        select 'Successful' -- for display
        END -- end of ELSE (after IF)
    END -- end of SP
    ```
    6.3. Use SP to withdraw money 

    `exec sp_withdraw 1, 880, null` for no error (user 1 has 1000 so enough to withdraw 880), run this and then see table `Customers` and `Withdrawals` now.

    `exec sp_withdraw 2, 510, null` for error. (user 2 has only 500, not enough to withdraw 510)

    `exec sp_withdraw 1, 50, @withdrawSucceededOrNot` to not only display on SSMS but also get the result to variable `@withdrawSucceededOrNot`


